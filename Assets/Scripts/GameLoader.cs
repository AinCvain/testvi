using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    [SerializeField]
    private UIManager _uIManager = null;

    [SerializeField]
    private CircleController _circleController = null;

    private async void Start()
    {
        SettingData settingData = null;

        do
        {
            _uIManager.LoadingPanelManager.ShowLoadingPanel("�������� ������");

            (LoadResult, SettingData) loadResult = await DataLoader.LoadData();

            if (loadResult.Item1 == LoadResult.FileIsNotExists)
            {
                _uIManager.LoadingPanelManager.ShowLoadingPanel("����� �� ����������, ������������ ����������");

                if (await DataLoader.DownloadData() == DownloadResult.WebException)
                {
                    _uIManager.LoadingPanelManager.ShowLoadingPanel("��� ������� � ��������� �������");
                    Debug.LogError("��");
                    await Task.Run(()=>Thread.Sleep(2500));
                    Debug.LogError("��");
                    Application.Quit();
                    return;
                }
            }
            else
            {
                settingData = loadResult.Item2;
                break;
            }
        } while (true);

        if (settingData.routePoints.Count <= 1)
        {
            _uIManager.LoadingPanelManager.ShowLoadingPanel("������������ ����� (����������� ������� ��� �����!)");
            await Task.Run(() => Thread.Sleep(2500));
            Application.Quit();
        }
        else
        {
            _uIManager.LoadingPanelManager.ShowLoadingPanel("���������� UI");
            _uIManager.GameInterfaceManager.SetTime(settingData.travelTime);
            _uIManager.GameInterfaceManager.SetLoopMode(settingData.isLoop);

            _uIManager.LoadingPanelManager.ShowLoadingPanel("������� ������ �������");
            _circleController.SetSetting(settingData);

            _uIManager.LoadingPanelManager.ShowLoadingPanel("��������� �������");
            _circleController.SetCircleIsReady(true);

            _uIManager.LoadingPanelManager.CloseLoadingPanel();
        }
    }
}
