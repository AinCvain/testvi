using TMPro;
using UnityEngine;

public class GameInterfaceManager : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _loopModeValue = null;

    [SerializeField]
    private TMP_Text _travelTimeValue = null;

    public void SetLoopMode(bool value)
    {
        _loopModeValue.text = value ? "��" : "���";
    }

    public void SetTime(float time)
    {
        _travelTimeValue.text = time.ToString() + " ���.";
    }
}
