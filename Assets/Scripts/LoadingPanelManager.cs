using TMPro;
using UnityEngine;

public class LoadingPanelManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _loadingPanel = null;

    [SerializeField]
    private TMP_Text _loadingPanelText = null;

    public void ShowLoadingPanel(string textForLoading)
    {
        _loadingPanel.SetActive(true);
        _loadingPanelText.text = textForLoading;
    }

    public void CloseLoadingPanel()
    {
        _loadingPanel.SetActive(false);
        _loadingPanelText.text = "";
    }
}
