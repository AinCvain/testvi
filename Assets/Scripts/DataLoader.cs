using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

public enum LoadResult
{
    FileIsNotExists, FileIsLoad 
}

public enum DownloadResult
{
    FileIsDownload, WebException
}

public class SettingData
{
    public bool isLoop;
    public float travelTime;
    public List<Vector2> routePoints;

    public SettingData(List<Vector2> routePoints, float travelTime, bool isLoop)
    {
        this.isLoop = isLoop;
        this.travelTime = travelTime;
        this.routePoints = routePoints;
    }
}

public class DataLoader
{
    public const string CONFIG_FILE_NAME = "Config.txt";

    public async static Task<DownloadResult> DownloadData()
    {
        WebClient webClient = new WebClient();
        try
        {
            await Task.Run(() =>
                webClient.DownloadFile("https://drive.google.com/uc?export=download&id=1XrcnaRoBa4SXJmNTU-4c--q-UDg3MGFo", CONFIG_FILE_NAME));

            return DownloadResult.FileIsDownload;
        }
        catch (WebException)
        {
            return DownloadResult.WebException;
        }

    }

    public async static Task<(LoadResult, SettingData)> LoadData()
    {
        List<Vector2> routePoints = new List<Vector2>();
        bool isLoop = false;
        float travelTime = 1;

        if (!File.Exists(CONFIG_FILE_NAME))
        {
            return (LoadResult.FileIsNotExists, null); 
        }

        using (StreamReader streamReader = new StreamReader(CONFIG_FILE_NAME, System.Text.Encoding.Default))
        {
            string line;

            while ((line = (await streamReader.ReadLineAsync())) != null)
            {
                if (line.Contains("loop = "))
                {
                    int loopValue = 0;
                    isLoop = int.TryParse(line.Split('=')[1].Trim(), out loopValue) && loopValue == 1;
                }
                else if (line.Contains("time = "))
                {

                    if (!float.TryParse(line.Split('=')[1].Trim(), out travelTime))
                    {
                        travelTime = 1;
                    }
                }
                else
                {
                    float xPoint;
                    float yPoint;
                    if (float.TryParse(line.Split(',')[0], out xPoint) && float.TryParse(line.Split(',')[1], out yPoint))
                    {
                        routePoints.Add(new Vector2(xPoint, yPoint));
                    }
                }
            }
        }
        return (LoadResult.FileIsLoad, new SettingData(routePoints, travelTime, isLoop));
    }
}
