using System.Collections.Generic;
using UnityEngine;

public class CircleController : MonoBehaviour
{
    private float _startTime = 0f;
    private bool _isMove = false;
    private bool _goToStartPoint = true;
    private Vector2 _startPosition;
    private List<Vector2> _routePoints = null;
    private float _travelTime = 0;
    private bool _isLoop = false;

    private bool _circleIsReady = false;

    public void SetSetting(SettingData settingData)
    {
        _routePoints = settingData.routePoints;
        _travelTime = settingData.travelTime;
        _isLoop = settingData.isLoop;
    }

    public void SetCircleIsReady(bool isReady)
    {
        _circleIsReady = isReady;
    }

    private void Update()
    {
        if (_circleIsReady)
        {
            if (_isMove)
            {
                float interpolateProgress;
              
                if (_goToStartPoint)
                {
                    interpolateProgress = (Time.time - _startTime) / 1;
                    transform.position = Vector2.Lerp(_startPosition, _routePoints[0], interpolateProgress);
                }
                else
                {
                    interpolateProgress = (Time.time - _startTime) / (_travelTime);
                    transform.position = Bezier.GetPoint(interpolateProgress, _routePoints);
                }

                if (interpolateProgress >= 1)
                {
                    if (_goToStartPoint)
                    {
                        _goToStartPoint = false;
                        _startTime = Time.time;
                        _startPosition = transform.position;
                        return;
                    }

                    if (_isLoop)
                    {
                        _startTime = Time.time;
                        _startPosition = transform.position;
                    }
                    else
                    {
                        _isMove = false;
                    }
                    _goToStartPoint = true;
                }
            }
            else if (Input.GetKey(KeyCode.Space))
            {
                _startTime = Time.time;
                _startPosition = transform.position;
                _isMove = true;
            }
        }
    }
}
