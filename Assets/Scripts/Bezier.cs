using System.Collections.Generic;
using UnityEngine;

public class Bezier : MonoBehaviour
{
    public static Vector2 GetPoint(float time, List<Vector2> points)
    { 
        List<Vector2> firstLines = new List<Vector2>();
        for (int i = 0; i < points.Count-1; i++)
        {
            firstLines.Add(Vector2.Lerp(points[i], points[i+1], time));
        }
        if (firstLines.Count != 1)
        {
            return GetPoint(time, firstLines);
        }

        return firstLines[0];
    }
}
