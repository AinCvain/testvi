using UnityEngine;

public class UIManager : MonoBehaviour
{
	[SerializeField]
	private LoadingPanelManager _loadingPanelManager = null;

	public LoadingPanelManager LoadingPanelManager { get { return _loadingPanelManager; } }

	[SerializeField]
	private GameInterfaceManager _gameInterfacManagerManager = null;

	public GameInterfaceManager GameInterfaceManager { get { return _gameInterfacManagerManager; } }

	private static UIManager _instance = null;

	private void Awake()
	{
		if (_instance == null)
		{
			_instance = this;
		}
		else if (_instance == this)
		{
			Destroy(gameObject);
		}
	}

	public static UIManager GetInstance()
	{
		return _instance;
	}
}
